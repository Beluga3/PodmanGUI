﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Podman_GUI.Processors
{
    class PodmanCommandProcessor
    {
        ProcessWindowStyle pws = ProcessWindowStyle.Hidden;
        //RuntimeParams
        private String _connection = null;
        private String _identity = null;
        private String _loglevel = null;
        private String[] _storageopt = null;
        private String _url = null;
        private String _version = null;
        private Dictionary<String, String[]> _runtimeparams = new Dictionary<String, String[]>();

        public String connection { get { return _connection; } set { _connection = value; _runtimeparams.Add("--connection", new[] {value}); } }
        public String identity { get { return _identity; } set { _identity = value; _runtimeparams.Add("--identity", new[] { value}); } }
        public String loglevel { get { return _loglevel; } set { _loglevel = value; _runtimeparams.Add("--loglevel", new[] { value }); } }
        public String[] storageopt { get { return _storageopt; } set { _storageopt = value; _runtimeparams.Add("--storageopt", value); } }
        public String url { get { return _url; } set { _url = value; _runtimeparams.Add("--url", new[] { value }); } }
        public String version { get { return _version; } set { _version = value; _runtimeparams.Add("--version", new[] { value }); } }


        //public static PodmanCommandProcessor podmancommandprocessor; //to call non-static methods from static methods
        public PodmanCommandProcessor() { }

        private void CommandRunner(String CommandAndOptions) {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = pws;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C " + CommandAndOptions + " " + _runtimeparams.ToString();
            process.StartInfo = startInfo;
            process.Start();
        } 

        public void Attach(String containername, String detachkeys=null, String latest=null, String nostdin=null, String sigproxy=null) {
            //usage:  Attach("name", latest:"latestimage");
            String CommandOption = " ";
            if(!String.IsNullOrEmpty(detachkeys)) { CommandOption += "--detach-keys " + detachkeys + " "; }
            if(!String.IsNullOrEmpty(latest)) { CommandOption += "--latest "; }
            if(!String.IsNullOrEmpty(nostdin)) { CommandOption += "--no-stdin "; }
            if(!String.IsNullOrEmpty(sigproxy)) { CommandOption += "--sig-proxy "; }
            
            try
            {
                String CommandText = "podman attach" + CommandOption + containername;
                CommandRunner(CommandText);
            } catch (Exception) { }
        }

        public void Build(String sourceUrl, String addhost=null, String allplatforms="false", String annotation=null, 
            String arch=null, String authfile=null, String buildarg=null, String cachefrom=null, String capadd=null, 
            String capdrop=null, String certdir=null, String cgroupparent=null, String cgroupns=null, String compress=null, 
            String cpuperiod=null, String cpuquota=null, String cpushares=null, String cpusetcpus=null, String cpusetmems=null, 
            String creds=null, String decryptionkey=null, String device=null, String disablecompression=null, String disablecontenttrust=null, 
            String dns=null, String dnsoption=null, String dnssearch=null, String file=null, String forcerm="true", String format=null, 
            String from=null, String help="false", String httpproxy="false", String iidfile=null, String ignorefile=null, String ipc=null, 
            String isolation=null, String jobs=null, String label=null, String layers="true", String logfile=null, String manifest=null, 
            String memory=null, String memoryswap=null, String network=null, String nocache="false", String os=null, String pid=null, 
            String platform=null, String pull="false", String quiet=null, String rm="true", String runtime=null, String secret=null, 
            String securityopt=null, String shmsize=null, String signby=null, String squash="false", String squashall="false", 
            String ssh=null, String stdin="false", String tag=null, String target=null, String timestamp=null, String tlsverify="false", 
            String ulimit=null, String unsetenv="false", String userns=null, String usernsuidmap=null, String usernsgidmap=null, 
            String usernsuidmapuser=null, String usernsgidmapgroup=null, String uts=null, String variant=null, String volume=null) {
            String CommandOption = " ";
            if(!String.IsNullOrEmpty(addhost)) { CommandOption += "--add-host " + addhost + " "; }
            if(allplatforms == "true") { CommandOption += "--all-platforms" + " "; }
            if(!String.IsNullOrEmpty(annotation)) { CommandOption += "--annotation " + annotation + " "; }
            if(!String.IsNullOrEmpty(arch)) { CommandOption += "--arch " + arch + " "; }
            if(!String.IsNullOrEmpty(authfile)) { CommandOption += "--authfile " + authfile + " "; }
            if(!String.IsNullOrEmpty(buildarg)) { CommandOption += "--build-arg " + buildarg + " "; }
            if(!String.IsNullOrEmpty(cachefrom)) { /*Not yet available on client*/}
            if(!String.IsNullOrEmpty(capadd)) { CommandOption += "--cap-add " + capadd + " "; }
            if(!String.IsNullOrEmpty(capdrop)) { CommandOption += "--cap-drop " + capdrop + " "; }
            if(!String.IsNullOrEmpty(certdir)) { CommandOption += "--cert-dir " + certdir + " "; }
            if(!String.IsNullOrEmpty(cgroupparent)) { CommandOption += "--cgroup-parent " + cgroupparent + " "; }
            if(!String.IsNullOrEmpty(cgroupns)) { CommandOption += "--cgroupns " + cgroupns + " "; }
            if(compress == "true") { /*Not yet available on client*/}
            if(!String.IsNullOrEmpty(cpuperiod)) { CommandOption += "--cpu-period " + cpuperiod + " "; }
            if(!String.IsNullOrEmpty(cpuquota)) { CommandOption += "--cpu-quota " + cpuquota + " "; }
            if(!String.IsNullOrEmpty(cpushares)) { CommandOption += "--cpu-shares " + cpushares + " "; }
            if(!String.IsNullOrEmpty(cpusetcpus)) { CommandOption += "--cpuset-cpus " + cpusetcpus + " "; }
            if(!String.IsNullOrEmpty(cpusetmems)) { CommandOption += "--cpuset-mems " + cpusetmems + " "; }
            if(!String.IsNullOrEmpty(creds)) { CommandOption += "--creds " + creds + " "; }
            if(!String.IsNullOrEmpty(decryptionkey)) { CommandOption += "--decryption-key " + decryptionkey + " "; }
            if(!String.IsNullOrEmpty(device)) { CommandOption += "--device " + device + " "; }
            if(!String.IsNullOrEmpty(disablecompression)) { CommandOption += "--disable-compression " + disablecompression + " "; }
            if(!String.IsNullOrEmpty(disablecontenttrust)) { CommandOption += "--disable-content-trust " + disablecontenttrust + " "; }
            if(!String.IsNullOrEmpty(dns)) { CommandOption += "--dns " + dns + " "; }
            if(!String.IsNullOrEmpty(dnsoption)) { CommandOption += "--dns-option " + dnsoption + " "; }
            if(!String.IsNullOrEmpty(dnssearch)) { CommandOption += "--dns-search " + dnssearch + " "; }
            if(!String.IsNullOrEmpty(file)) { CommandOption += "--file " + file + " "; }
            if(forcerm == "true") { CommandOption += "--force-rm "; }
            if(!String.IsNullOrEmpty(format)) { CommandOption += "--format " + format + " "; }
            if(!String.IsNullOrEmpty(from)) { CommandOption += "--from " + from + " "; }
            if(help == "true") { CommandOption += "--help "; }
            if(httpproxy == "true") { CommandOption += "--http-proxy "; }
            if(!String.IsNullOrEmpty(iidfile)) { CommandOption += "--iidfile " + iidfile + " "; }
            if(!String.IsNullOrEmpty(ignorefile)) { CommandOption += "--ignorefile " + ignorefile + " "; }
            if(!String.IsNullOrEmpty(ipc)) { CommandOption += "--ipc " + ipc + " "; }
            if(!String.IsNullOrEmpty(isolation)) { CommandOption += "--isolation " + isolation + " "; }
            if(!String.IsNullOrEmpty(jobs)) { CommandOption += "--jobs " + jobs + " "; }
            if(!String.IsNullOrEmpty(label)) { CommandOption += "--label " + label + " "; }
            if(layers == "false") { CommandOption += "--layers "; }
            if(!String.IsNullOrEmpty(logfile)) { CommandOption += "--logfile " + logfile + " "; }
            if(!String.IsNullOrEmpty(manifest)) { CommandOption += "--manifest " + manifest + " "; }
            if(!String.IsNullOrEmpty(memory)) { CommandOption += "--memory " + memory + " "; }
            if(!String.IsNullOrEmpty(memoryswap)) { CommandOption += "--memory-swap " + memoryswap + " "; }
            if(!String.IsNullOrEmpty(network)) { CommandOption += "--network " + network + " "; }
            if(nocache == "true") { CommandOption += "--nocache "; }
            if(!String.IsNullOrEmpty(os)) { CommandOption += "--os " + os + " "; }
            if(!String.IsNullOrEmpty(pid)) { CommandOption += "--pid " + pid + " "; }
            if(!String.IsNullOrEmpty(platform)) { CommandOption += "--platform " + platform + " "; }
            if(pull != "false") { CommandOption += "--pull " + pull + " "; }
            if(quiet == "true") { CommandOption += "--quiet "; }
            if(rm == "false") { CommandOption += "--rm " + rm + " "; }
            if(!String.IsNullOrEmpty(runtime)) { CommandOption += "--runtime " + runtime + " "; }
            if(!String.IsNullOrEmpty(secret)) { CommandOption += "--secret " + secret + " "; }
            if(!String.IsNullOrEmpty(securityopt)) { CommandOption += "--security-opt " + securityopt + " "; }
            if(!String.IsNullOrEmpty(shmsize)) { CommandOption += "--shm-size " + shmsize + " "; }
            if(!String.IsNullOrEmpty(signby)) { CommandOption += "--sign-by " + signby + " "; }
            if(squash == "true") { CommandOption += "--squash "; }
            if(squashall == "true") { CommandOption += "--squash-all "; }
            if(!String.IsNullOrEmpty(ssh)) { CommandOption += "--ssh " + ssh + " "; }
            if(stdin == "true") { CommandOption += "--stdin "; }
            if(!String.IsNullOrEmpty(tag)) { CommandOption += "--tag " + tag + " "; }
            if(!String.IsNullOrEmpty(target)) { CommandOption += "--target " + target + " "; }
            if(!String.IsNullOrEmpty(timestamp)) { CommandOption += "--timestamp " + timestamp + " "; }
            if(tlsverify == "true") { CommandOption += "--tls-verify"; }
            if(!String.IsNullOrEmpty(ulimit)) { CommandOption += "--ulimit " + ulimit + " "; }
            if(unsetenv == "true") { CommandOption += "--unsetenv "; }
            if(!String.IsNullOrEmpty(userns)) { CommandOption += "--userns " + userns + " "; }
            if(!String.IsNullOrEmpty(usernsuidmap)) { CommandOption += "--userns-uid-map " + usernsuidmap + " "; }
            if(!String.IsNullOrEmpty(usernsgidmap)) { CommandOption += "--userns-gid-map " + usernsgidmap + " "; }
            if(!String.IsNullOrEmpty(usernsuidmapuser)) { CommandOption += "--userns-uid-map-user " + usernsuidmapuser + " "; }
            if(!String.IsNullOrEmpty(usernsgidmapgroup)) { CommandOption += "--userns-gid-map-group " + usernsgidmapgroup + " "; }
            if(!String.IsNullOrEmpty(uts)) { CommandOption += "--uts " + uts + " "; }
            if(!String.IsNullOrEmpty(variant)) { CommandOption += "--variant " + variant + " "; }
            if(!String.IsNullOrEmpty(volume)) { CommandOption += "--volume " + volume + " "; }

            try
            {
                String CommandText = "podman build" + CommandOption + sourceUrl;
                CommandRunner(CommandText);
            } catch (Exception)
            {
                try
                {
                    CommandOption += "--annotation run.oci.keep_original_groups=1";
                    String CommandText = "podman build" + CommandOption + sourceUrl;
                    CommandRunner(CommandText);
                }
                catch (Exception) { }
            }
        }
        public void Commit(String author = null, String change = null, String format = null, String iidfile = null,
            String includevolumes = "false", String message = null, String pause = "false", String quiet = "false") {
            String CommandOption = " ";
            if (!String.IsNullOrEmpty(author)) { CommandOption += "--author " + author + " "; }
            if (!String.IsNullOrEmpty(change)) { CommandOption += "--change " + change + " "; }
            if (!String.IsNullOrEmpty(format)) { CommandOption += "--format " + format + " "; }
            if (!String.IsNullOrEmpty(iidfile)) { CommandOption += "--iidfile" + iidfile + " "; }
            if (!String.IsNullOrEmpty(includevolumes)) { CommandOption += "--include-volume " + includevolumes + " "; }
            if (!String.IsNullOrEmpty(message)) { CommandOption += "--message " + message + " "; }
            if (pause == "true") { CommandOption += "--pause "; }
            if (quiet == "true") { CommandOption += "--quite "; }

            try
            {
                String CommandText = "podman commit" + CommandOption;
                CommandRunner(CommandText);
            }
            catch (Exception) { }
        }
        public void Container() { } //TODO
        public void Copy(String sourcecontainerpath, String destcontainerpath, String archive=null) /*cp*/ {
            String CommandOption = " ";
            if (!String.IsNullOrEmpty(archive)) { CommandOption += "--archive " + archive + " "; }

            try
            {
                String CommandText = "podman cp" + CommandOption + sourcecontainerpath + " " + destcontainerpath;
                CommandRunner(CommandText);
            }
            catch (Exception) { }
        }
        public void Create(String ImageName, String addhost = null, String annotation = null, String arch = null,
            String attach = null, String authfile = null, String blkioweight = null, String blkioweightdevice = null,
            String capadd = null, String capdrop = null, String cgroupns = null, String cgroups = null, String cgroupparent = null,
            String cgroupconf = null, String cidfile = null, String conmonpidfile = null, String cpuperiod = null, String cpuquota = null,
            String cpurtperiod = null, String cpurtruntime = null, String cpushares = null, String cpus = null, String cpusetcpus = null,
            String cpusetmems = null, String device = null, String devicecgrouprule = null, String devicereadbps = null,
            String devicereadiops = null, String devicewritebps = null, String devicewriteiops = null, String dns = null,
            String dnsopt = null, String dnssearch = null, String entrypoint = null, String env = null, String envhost = null,
            String envfile = null, String expose = null, String gidmap = null, String groupadd = null, String healthcmd = null,
            String healthinterval = null, String healthretries = null, String healthstartperiod = null, String healthtimeout = null, String hostname = null, String hostuser = null, String help = "false",
            String httpproxy = "true", String imagevolume = "bind", String init = "false", String initctr = null, String initpath = null,
            String interactive = "false", String ip = null, String ip6 = null, String ipc = null, String label = null, String labelfile = null,
            String logdriver = "k8s-file", String logopt = null, String macaddress = null, String memory = null, String memoryreservation = null,
            String memoryswap = null, String memoryswapiness = null, String mount = null, String name = null, String network = null,
            String networkalias = null, String nohealthcheck = "false", String nohosts = "false", String oomkilldisable = "false",
            String oomscoreadj = null, String os = null, String personality = null, String pid = null, String pidslimit = null,
            String platform = null, String pod = null, String podidfile = null, String privileged = "false", String publish = "false", String publishall = "false",
            String pull = "missing", String quiet = "false", String read_only = "false", String readonlytmpfs = "true", String replace = "false",
            String requires = null, String restart = "no", String rm = "false", String rootfs = null, String sdnotify = "container",
            String seccomppolicy = null, String secret = null, String securityopt = null, String shmsize = null, String stopsignal = "SIGTERM", 
            String stoptimeout = "10", String subgidname = null, String subuidname = null, String sysctl = null, String systemd = "true", 
            String timeout = null, String tlsverify = "true", String tmpfs = null, String tty = "false", String tz = null, String umask = "0022", 
            String unsetenv = null, String unsetenvall = "false", String uidmap = null, String ulimit = null, String user = null, 
            String userns = null, String uts = "private", String variant = null, String volume = null, String volumesfrom = null, 
            String workdir = null, String pidfile = null) {
            String CommandOption = " ";
            if(!String.IsNullOrEmpty(addhost)) { CommandOption += "--add-host " + addhost + " "; }
            if (!String.IsNullOrEmpty(annotation)) { CommandOption += "--annotation " + annotation + " "; }
            if (!String.IsNullOrEmpty(arch)) { CommandOption += "--arch " + arch + " "; }
            if (!String.IsNullOrEmpty(attach)) { CommandOption += "--attach " + attach + " "; }
            if (!String.IsNullOrEmpty(authfile)) { CommandOption += "--authfile " + authfile + " "; }
            if (!String.IsNullOrEmpty(blkioweight)) { CommandOption += "--blkio-weight " + blkioweight + " "; }
            if (!String.IsNullOrEmpty(blkioweightdevice)) { CommandOption += "--blkio-weight-device " + blkioweightdevice + " "; }
            if (!String.IsNullOrEmpty(capadd)) { CommandOption += "--cap-add " + capadd + " "; }
            if (!String.IsNullOrEmpty(capdrop)) { CommandOption += "--cap-drop " + capdrop + " "; }
            if (!String.IsNullOrEmpty(cgroupns)) { CommandOption += "--cgroupns " + cgroupns + " "; }
            if (!String.IsNullOrEmpty(cgroups)) { CommandOption += "--cgroups " + cgroups + " "; }
            if (!String.IsNullOrEmpty(cgroupparent)) { CommandOption += "--cgroup-parent " + cgroupparent + " "; }
            if (!String.IsNullOrEmpty(cgroupconf)) { CommandOption += "--cgroup-conf " + cgroupconf + " "; }
            if (!String.IsNullOrEmpty(cidfile)) { CommandOption += "--cidfile " + cidfile + " "; }
            if (!String.IsNullOrEmpty(conmonpidfile)) { CommandOption += "--conmon-pidfile " + conmonpidfile + " "; }
            if (!String.IsNullOrEmpty(cpuperiod)) { CommandOption += "--cpu-period " + cpuperiod + " "; }
            if (!String.IsNullOrEmpty(cpuquota)) { CommandOption += "--cpu-quota " + cpuquota + " "; }
            if (!String.IsNullOrEmpty(cpurtperiod)) { CommandOption += "--cpu-rt-period " + cpurtperiod + " "; }
            if (!String.IsNullOrEmpty(cpurtruntime)) { CommandOption += "--cpu-rt-runtime " + cpurtruntime + " "; }
            if (!String.IsNullOrEmpty(cpushares)) { CommandOption += "--cpu-shares " + cpushares + " "; }
            if (!String.IsNullOrEmpty(cpus)) { CommandOption += "--cpus " + cpus + " "; }
            if (!String.IsNullOrEmpty(cpusetcpus)) { CommandOption += "--cpuset-cpus " + cpusetcpus + " "; }
            if (!String.IsNullOrEmpty(cpusetmems)) { CommandOption += "--cpuset-mems " + cpusetmems + " "; }
            if (!String.IsNullOrEmpty(device)) { CommandOption += "--device " + device + " "; }
            if (!String.IsNullOrEmpty(devicecgrouprule)) { CommandOption += "--device-cgroup-rule " + devicecgrouprule + " "; }
            if (!String.IsNullOrEmpty(devicereadbps)) { CommandOption += "--device-read-bps " + devicereadbps + " "; }
            if (!String.IsNullOrEmpty(devicereadiops)) { CommandOption += "--device-read-iops " + devicereadiops + " "; }
            if (!String.IsNullOrEmpty(devicewritebps)) { CommandOption += "--device-write-bps " + devicewritebps + " "; }
            if (!String.IsNullOrEmpty(devicewriteiops)) { CommandOption += "--device-write-iops " + devicewriteiops + " "; }
            if (!String.IsNullOrEmpty(dns)) { CommandOption += "--dns " + dns + " "; }
            if (!String.IsNullOrEmpty(dnsopt)) { CommandOption += "--dns-opt " + dnsopt + " "; }
            if (!String.IsNullOrEmpty(dnssearch)) { CommandOption += "--dns-search " + dnssearch + " "; }
            if (!String.IsNullOrEmpty(entrypoint)) { CommandOption += "--entrypoint " + entrypoint + " "; }
            if (!String.IsNullOrEmpty(env)) { CommandOption += "--env " + env + " "; }
            if (!String.IsNullOrEmpty(envhost)) { CommandOption += "--env-host " + envhost + " "; }
            if (!String.IsNullOrEmpty(envfile)) { CommandOption += "--env-file " + envfile + " "; }
            if (!String.IsNullOrEmpty(expose)) { CommandOption += "--expose " + expose + " "; }
            if (!String.IsNullOrEmpty(gidmap)) { CommandOption += "--gidmap " + gidmap + " "; }
            if (!String.IsNullOrEmpty(groupadd)) { CommandOption += "--group-add " + groupadd + " "; }
            if (!String.IsNullOrEmpty(healthcmd)) { CommandOption += "--health-cmd " + healthcmd + " "; }
            if (!String.IsNullOrEmpty(healthinterval)) { CommandOption += "--health-interval " + healthinterval + " "; }
            if (!String.IsNullOrEmpty(healthretries)) { CommandOption += "--health-retries " + healthretries + " "; }
            if (!String.IsNullOrEmpty(healthstartperiod)) { CommandOption += "--health-start-period " + healthstartperiod + " "; }
            if (!String.IsNullOrEmpty(healthtimeout)) { CommandOption += "--health-timeout " + healthtimeout + " "; }
            if (!String.IsNullOrEmpty(hostname)) { CommandOption += "--hostname " + hostname + " "; }
            if (!String.IsNullOrEmpty(hostuser)) { CommandOption += "--hostuser " + hostuser + " "; }
            if (help == "true") { CommandOption += "--help "; }
            if (httpproxy == "false") { CommandOption += "--http-proxy " + httpproxy + " "; }
            if (imagevolume != "bind") { CommandOption += "--image-volume " + imagevolume + " "; }
            if (init == "true") { CommandOption += "--init "; }
            if (!String.IsNullOrEmpty(initctr)) { CommandOption += "--init-ctr " + initctr + " "; }
            if (!String.IsNullOrEmpty(initpath)) { CommandOption += "--init-path " + initpath + " "; }
            if (interactive == "true") { CommandOption += "--interactive "; }
            if (!String.IsNullOrEmpty(ip)) { CommandOption += "--ip " + ip + " "; }
            if (!String.IsNullOrEmpty(ip6)) { CommandOption += "--ip6 " + ip6 + " "; }
            if (!String.IsNullOrEmpty(ipc)) { CommandOption += "--ipc " + ipc + " "; }
            if (!String.IsNullOrEmpty(label)) { CommandOption += "--label " + label + " "; }
            if (!String.IsNullOrEmpty(labelfile)) { CommandOption += "--label-file " + label + " "; }
            if (logdriver != "k8s-file") { CommandOption += "--log-driver " + logdriver + " "; }
            if (!String.IsNullOrEmpty(logopt)) { CommandOption += "--log-opt " + logopt + " "; }
            if (!String.IsNullOrEmpty(macaddress)) { CommandOption += "--mac-address " + macaddress + " "; }
            if (!String.IsNullOrEmpty(memory)) { CommandOption += "--memory " + memory + " "; }
            if (!String.IsNullOrEmpty(memoryreservation)) { CommandOption += "--memory-reservation " + memoryreservation + " "; }
            if (!String.IsNullOrEmpty(memoryswap)) { CommandOption += "--memory-swap " + memoryswap + " "; }
            if (!String.IsNullOrEmpty(memoryswapiness)) { CommandOption += "--memory-swapiness " + memoryswapiness + " "; }
            if (!String.IsNullOrEmpty(mount)) { CommandOption += "--mount " + mount + " "; }
            if (!String.IsNullOrEmpty(name)) { CommandOption += "--name " + name + " "; }
            if (!String.IsNullOrEmpty(network)) { CommandOption += "--network " + network + " "; }
            if (!String.IsNullOrEmpty(networkalias)) { CommandOption += "--network-alias " + networkalias + " "; }
            if (nohealthcheck == "true") { CommandOption += "--no-healthcheck "; }
            if (nohosts == "true") { CommandOption += "--no-hosts "; }
            if (oomkilldisable == "true") { CommandOption += "--oom-kill-disable "; }
            if (!String.IsNullOrEmpty(oomscoreadj)) { CommandOption += "--oom-score-adj " + oomscoreadj + " "; }
            if (!String.IsNullOrEmpty(os)) { CommandOption += "--os " + os + " "; }
            if (!String.IsNullOrEmpty(personality)) { CommandOption += "--personality " + personality + " "; }
            if (!String.IsNullOrEmpty(pid)) { CommandOption += "--pid " + pid + " "; }
            if (!String.IsNullOrEmpty(pidslimit)) { CommandOption += "--pids-limit " + pidslimit + " "; }
            if (!String.IsNullOrEmpty(platform)) { CommandOption += "--platform " + platform + " "; }
            if (!String.IsNullOrEmpty(pod)) { CommandOption += "--pod " + pod + " "; }
            if (!String.IsNullOrEmpty(podidfile)) { CommandOption += "--pod-id-file " + podidfile + " "; }
            if (privileged == "true") { CommandOption += "--privileged "; }
            if (publish == "true") { CommandOption += "--publish "; }
            if (publishall == "true") { CommandOption += "--publish-all "; }
            if (pull != "missing") { CommandOption += "--pull " + pull + " "; }
            if (quiet == "true") { CommandOption += "--quiet "; }
            if (read_only == "true") { CommandOption += "--read-only "; }
            if (readonlytmpfs == "false") { CommandOption += "read-only-tmpfs " + readonlytmpfs + " "; }
            if (replace == "true") { CommandOption += "--replace "; }
            if (!String.IsNullOrEmpty(requires)) { CommandOption += "--requires " + requires + " "; }
            if (restart != "no") { CommandOption += "--restart " + restart + " "; }
            if (rm == "true") { CommandOption += "--rm "; }
            if (!String.IsNullOrEmpty(rootfs)) { CommandOption += "--rootfs " + rootfs + " "; }
            if (sdnotify != "container") { CommandOption += "--sdnotify " + sdnotify + " "; }
            if (!String.IsNullOrEmpty(seccomppolicy)) { CommandOption += "--seccomp-policy " + seccomppolicy + " "; }
            if (!String.IsNullOrEmpty(secret)) { CommandOption += "--secret " + secret + " "; }
            if (!String.IsNullOrEmpty(securityopt)) { CommandOption += "--security-opt " + securityopt + " "; }
            if (!String.IsNullOrEmpty(shmsize)) { CommandOption += "--shm-size " + shmsize + " "; }
            if (stopsignal != "SIGTERM") { CommandOption += "--stop-signal " + stopsignal + " "; }
            if (!String.IsNullOrEmpty(stoptimeout)) { CommandOption += "--stop-timeout " + stoptimeout + " "; }
            if (!String.IsNullOrEmpty(subgidname)) { CommandOption += "--subgidname " + subgidname + " "; }
            if (!String.IsNullOrEmpty(subuidname)) { CommandOption += "--subuidname " + subuidname + " "; }
            if (!String.IsNullOrEmpty(sysctl)) { CommandOption += "--sysctl " + sysctl + " "; }
            if (systemd != "true") { CommandOption += "--systemd " + systemd + " "; }
            if (!String.IsNullOrEmpty(timeout)) { CommandOption += "--timeout " + timeout + " "; }
            if (tlsverify != "true") { CommandOption += "--tls-verify "; }
            if (!String.IsNullOrEmpty(tmpfs)) { CommandOption += "--tmpfs " + tmpfs + " "; }
            if (tty != "false") { CommandOption += "--tty "; }
            if (!String.IsNullOrEmpty(tz)) { CommandOption += "--tz " + tz + " "; }
            if (umask != "0022") { CommandOption += "--umask " + umask + " "; }
            if (!String.IsNullOrEmpty(unsetenv)) { CommandOption += "--unsetenv " + unsetenv + " "; }
            if (unsetenvall != "false") { CommandOption += "--unsetenv-all " + unsetenvall + " "; }
            if (!String.IsNullOrEmpty(uidmap)) { CommandOption += "--uidmap " + uidmap + " "; }
            if (!String.IsNullOrEmpty(ulimit)) { CommandOption += "--ulimit " + ulimit + " "; }
            if (!String.IsNullOrEmpty(user)) { CommandOption += "--user " + user + " "; }
            if (!String.IsNullOrEmpty(userns)) { CommandOption += "--userns " + userns + " "; }
            if (uts != "private") { CommandOption += "--uts " + uts + " "; }
            if (!String.IsNullOrEmpty(variant)) { CommandOption += "--variant " + variant + " "; }
            if (!String.IsNullOrEmpty(volume)) { CommandOption += "--volume " + volume + " "; }
            if (!String.IsNullOrEmpty(volumesfrom)) { CommandOption += "--volumes-from " + volumesfrom + " "; }
            if (!String.IsNullOrEmpty(workdir)) { CommandOption += "--workdir " + workdir + " "; }
            if (!String.IsNullOrEmpty(pidfile)) { CommandOption += "--pidfile " + pidfile + " "; }

            try
            {
                String CommandText = "podman create" + CommandOption + ImageName;
                CommandRunner(CommandText);
            }
            catch (Exception) { }
        }
        public void Diff() { }
        public void Events() { }
        public void Exec() { }
        public void Export() { }
        public void Generate() { }
        public void Healthcheck() { }
        //public void Help() { }
        public void History() { }
        public void Image() { } //Manage images
        public void Images() { } //List images
        public void Import() { }
        public void Info() { } //Display system info
        public void Init() { }
        public void Inspect() { }
        public void Kill() { }
        public void List() { } //ps
        public void Load() { }
        public void Login() { }
        public void Logout() { }
        public void Logs() { }
        public void Manifest() { }
        public void Network() { }
        public void Pause() { }
        public void Play() { }
        public void Pod() { }
        public void Port() { }
        public void Pull() { }
        public void Push() { }
        public void Rename() { }
        public void Restart() { }
        public void RemoveContainer() { } //rm
        public void RemoveImage() { } //rmi
        public void Run() { }
        public void Save() { }
        public void Search() { }
        public void Secrets() { }
        public void Start() { }
        public void Stats() { }
        public void Stop() { }
        public void System() { }
        public void Tag() { }
        public void Top() { }
        public void Unpause() { }
        public void Untag() { }
        public void Version() { }
        public void Volume() { }
        public void Wait() { }
    }
}
